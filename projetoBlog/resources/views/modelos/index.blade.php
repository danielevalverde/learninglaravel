

@extends('layouts.app')

@section('content')

    <div class="container">

        <table class="table">
            <thead>
                <tr>
                <th scope="col">Id</th>
                <th scope="col">Modelo</th>
                <th scope="col">Status</th>
                 <th scope="col">Funções</th>
                </tr>
            </thead>
            <tbody>
              @foreach ($modelos as $modelo)
            <tr>
                <th scope="row"> {{$modelo->id}}</th>
                    <td>  Modelo:  {{$modelo->modelo}}</td>
                    <td>   Status: {{$modelo->status}} </td>
                    <td>
                        <a href="{{route('modelos.show',$modelo->id)}}">   Visualizar | </a>
                        <a href="{{route('modelos.edit',$modelo->id)}}"> Editar | </a>
                        <a href="{{route('modelos.destroy',$modelo->id)}}"> Deletar </a>
                    </td>
              
            </tr>
            @endforeach 
                </tbody>
        </table>

    </div>

@endsection