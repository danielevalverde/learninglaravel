@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('modelos.update',$modelo->id)}}" class="form-horizontal" method="POST"> 
    @csrf
    @method('PUT')
    <div class="row">
        <label for="modelo"> Modelo do veículo </label>
        <input type ="text" id="modelo" name="modelo" class ="form-control" value="{{old('modelo', $modelo->modelo)}}"> 
    </div>
    <div class="row">
        <label for="status">Status </label>
        <input type ="text" id="status" name="status" class ="form-control" value="{{old('modelo', $modelo->status)}}"> 
    </div>
      <div class="row">
        
        <input type ="submit" class ="form-control" value ="Atualizar">  
    </div>
    </form>
 </div>
@endsection