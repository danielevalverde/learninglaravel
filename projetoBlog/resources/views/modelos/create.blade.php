@extends('layouts.app')

@section('content')
<div class="container">
    <form action="{{route('modelos.store')}}" class="form-horizontal" method="POST"> 
    @csrf
    <div class="row">
        <label for="modelo"> Modelo do veículo </label>
        <input type ="text" id="modelo" name="modelo" class ="form-control"> 
    </div>
    <div class="row">
        <label for="status">Status </label>
        <input type ="text" id="status" name="status" class ="form-control"> 
    </div>
      <div class="row">
        
        <input type ="submit" class ="form-control" value ="Cadastrar">  
    </div>
    </form>
 </div>
@endsection